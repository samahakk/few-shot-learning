from flair.data import Corpus
from flair.models.text_classification_model import TARSClassifier
from flair.trainers import ModelTrainer
from flair.datasets import CSVClassificationCorpus

column_name_map = {0: "label", 1: "text"}

tasks_list = ('topic_huffpost_oneword',
              'topic_huffpost_interpretation', 'topic_huffpost_wordnet')
task = iter(tasks_list)

data_folders = [
    '/vol/fob-vol7/nebenf19/samahakk/test/few-shot-learning/topic_huffpost/oneword',
    '/vol/fob-vol7/nebenf19/samahakk/test/few-shot-learning/topic_huffpost/interpretation',
    '/vol/fob-vol7/nebenf19/samahakk/test/few-shot-learning/topic_huffpost/wordnet'
]

for data_folder in data_folders:
    corpus: Corpus = CSVClassificationCorpus(
        column_name_map = column_name_map,
        data_folder=data_folder,
        test_file=None,
        skip_header = True,
        delimiter=','
    )

    tars = TARSClassifier(task_name=next(task), label_dictionary=corpus.make_label_dictionary())

    trainer = ModelTrainer(tars, corpus)

    trainer.train(base_path=data_folder,
    learning_rate=0.02,
    mini_batch_size=16,
    mini_batch_chunk_size=4,
    max_epochs=10,
    train_with_test=True)